# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-dialer package.
#
# Emir SARI <emir_sari@icloud.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-dialer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-19 00:47+0000\n"
"PO-Revision-Date: 2023-03-04 14:09+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Emir SARI"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "emir_sari@icloud.com"

#: src/main.cpp:136 src/qml/main.qml:35
#, kde-format
msgid "Phone"
msgstr "Telefon"

#: src/main.cpp:138
#, kde-format
msgid "Plasma phone dialer"
msgstr "Plasma telefon çeviricisi"

#: src/main.cpp:140
#, kde-format
msgid "© 2015-2022 KDE Community"
msgstr "© 2015-2022 KDE Topluluğu"

#: src/main.cpp:143
#, kde-format
msgid "Alexey Andreyev"
msgstr "Alexey Andreyev"

#: src/qml/call/AsymmetricAnswerSwipe.qml:86
#, kde-format
msgid "Swipe to accept"
msgstr "Kabul etmek için kaydırın"

#: src/qml/call/CallPage.qml:35 src/qml/call/InCallInlineMessage.qml:14
#, kde-format
msgid "Active call list"
msgstr "Etkin arama listesi"

#: src/qml/call/CallPage.qml:108
#, kde-format
msgid "Incoming..."
msgstr "Gelen arama..."

#: src/qml/call/CallPage.qml:111
#, kde-format
msgid "Calling..."
msgstr "Arıyor..."

#: src/qml/call/CallPage.qml:134
#, kde-format
msgid "Keypad"
msgstr "Düğme takımı"

#: src/qml/call/CallPage.qml:154
#, kde-format
msgid "Speaker"
msgstr "Hoparlör"

#: src/qml/call/CallPage.qml:168
#, kde-format
msgid "Mute"
msgstr "Sessize al"

#: src/qml/call/InCallInlineMessage.qml:18
#, kde-format
msgid "View"
msgstr "Görünüm"

#: src/qml/components/BottomToolbar.qml:30 src/qml/components/Sidebar.qml:31
#, kde-format
msgid "History"
msgstr "Geçmiş"

#: src/qml/components/BottomToolbar.qml:52 src/qml/components/Sidebar.qml:49
#: src/qml/ContactsPage.qml:19
#, kde-format
msgid "Contacts"
msgstr "Kişiler"

#: src/qml/components/BottomToolbar.qml:74 src/qml/components/Sidebar.qml:67
#: src/qml/DialerPage.qml:27
#, kde-format
msgid "Dialer"
msgstr "Çevirici"

#: src/qml/components/Sidebar.qml:91 src/qml/ContactsPage.qml:30
#: src/qml/DialerPage.qml:57 src/qml/HistoryPage.qml:42
#: src/qml/settings/SettingsPage.qml:20
#, kde-format
msgid "Settings"
msgstr "Ayarlar"

#: src/qml/components/Sidebar.qml:110
#, kde-format
msgid "Quit"
msgstr "Çık"

#: src/qml/ContactsPage.qml:106
#, kde-format
msgid "Select number to call"
msgstr "Aranacak numarayı seçin"

#: src/qml/ContactsPage.qml:115
#, kde-format
msgid "No contacts have a phone number set"
msgstr "Numarası kayıtlı olan kimse yok"

#: src/qml/DialerPage.qml:88
#, kde-format
msgid "Modem devices are not found"
msgstr "Modem aygıtı bulunamadı"

#: src/qml/DialerPage.qml:97
#, kde-format
msgid "Voicemail number couldn't be found"
msgstr "Telesekreter numarası bulunamadı"

#: src/qml/history/HistoryDelegate.qml:61
#, kde-format
msgid "Duration: %1"
msgstr "Arama süresi: %1"

#: src/qml/HistoryPage.qml:18
#, kde-format
msgid "Call History"
msgstr "Arama Geçmişi"

#: src/qml/HistoryPage.qml:33
#, kde-format
msgid "Clear history"
msgstr "Geçmişi temizle"

#: src/qml/HistoryPage.qml:89
#, kde-format
msgid "No recent calls"
msgstr "Arama yok"

#: src/qml/ImeiSheet.qml:18
#, kde-format
msgid "IMEI"
msgid_plural "IMEIs"
msgstr[0] "IMEI"
msgstr[1] "IMEI'ler"

#: src/qml/ImeiSheet.qml:30
#, kde-format
msgid "No IMEIs found"
msgstr "Hiçbir IMEI bulunamadı"

#: src/qml/settings/CallBlockSettingsPage.qml:21
#, kde-format
msgid "Adaptive Call Blocking"
msgstr "Uyarlanabilir Arama Engelleme"

#: src/qml/settings/CallBlockSettingsPage.qml:39
#, kde-format
msgid "Ignore calls from unknown numbers"
msgstr "Bilinmeyen numaralardan olan aramaları yok say"

#: src/qml/settings/CallBlockSettingsPage.qml:45
#, kde-format
msgid "When a call is an incoming from an unknown number"
msgstr "Bilinmeyen bir numaradan bir arama geldiğinde"

#: src/qml/settings/CallBlockSettingsPage.qml:53
#, kde-format
msgid "Immediately hang up"
msgstr "Anında kapat"

#: src/qml/settings/CallBlockSettingsPage.qml:58
#, kde-format
msgid "Ring without notification"
msgstr "Bildirim olmadan çaldır"

#: src/qml/settings/CallBlockSettingsPage.qml:63
#, kde-format
msgid "Ring with silent notification"
msgstr "Sessiz bildirimle çaldır"

#: src/qml/settings/CallBlockSettingsPage.qml:71
#, kde-format
msgid "Allowed exceptions"
msgstr "İzin verilen istisnalar"

#: src/qml/settings/CallBlockSettingsPage.qml:80
#, kde-format
msgid "Anonymous numbers"
msgstr "Bilinmeyen numaralar"

#: src/qml/settings/CallBlockSettingsPage.qml:87
#, kde-format
msgid "Existing outgoing call to number"
msgstr "Bir numaraya var olan giden arama"

#: src/qml/settings/CallBlockSettingsPage.qml:94
#, kde-format
msgid "Callback within"
msgstr "Geri dönme zamanı:"

#: src/qml/settings/CallBlockSettingsPage.qml:112
#, kde-format
msgid "minutes"
msgstr "dakika"

#: src/qml/settings/CallBlockSettingsPage.qml:122
#, kde-format
msgid "Allowed phone number exceptions"
msgstr "İzin verilen telefon numarası istisnaları"

#: src/qml/settings/CallBlockSettingsPage.qml:142
#: src/qml/settings/CallBlockSettingsPage.qml:171
#, kde-format
msgid "Add new pattern"
msgstr "Yeni dizgi ekle"

#: src/qml/settings/SettingsPage.qml:41
#, kde-format
msgid "About"
msgstr "Hakkında"

#: src/qml/settings/SettingsPage.qml:50
#, kde-format
msgid "Adaptive call blocking"
msgstr "Uyarlanabilir arama engelleme"

#: src/qml/settings/SettingsPage.qml:59
#, kde-format
msgid "Incoming call screen appearance"
msgstr "Gelen arama ekranı görünüşü"

#: src/qml/settings/SettingsPage.qml:60
#, kde-format
msgid "Buttons"
msgstr "Düğmeler"

#: src/qml/settings/SettingsPage.qml:60
#, kde-format
msgid "Symmetric Swipe"
msgstr "Simetrik Kaydır"

#: src/qml/settings/SettingsPage.qml:60
#, kde-format
msgid "Asymmetric Swipe"
msgstr "Asimetrik Kaydır"

#: src/qml/USSDSheet.qml:40
#, kde-format
msgid "USSD Error"
msgstr "USSD Hatası"

#: src/qml/USSDSheet.qml:40
#, kde-format
msgid "USSD Message"
msgstr "USSD İletisi"

#: src/qml/USSDSheet.qml:49
#, kde-format
msgid "Write response..."
msgstr "Yanıt yaz..."

#: src/qml/USSDSheet.qml:54
#, kde-format
msgid "Send"
msgstr "Gönder"

#: src/qml/USSDSheet.qml:70
#, kde-format
msgid "Cancel USSD session"
msgstr "USSD oturumunu iptal et"

#~ msgid "Numbers matching a pattern:"
#~ msgstr "Bir dizgiyle eşleşen numaralar:"

#~ msgid "Answer control for the incoming screen"
#~ msgstr "Gelen arama ekranı için yanıtlama denetimi"

#~ msgid "Other"
#~ msgstr "Diğer"

#~ msgid "More Info:"
#~ msgstr "Daha Fazla Bilgi:"
